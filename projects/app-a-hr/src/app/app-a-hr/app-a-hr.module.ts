import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppAHrRoutingModule } from './app-a-hr-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AppAHrRoutingModule
  ]
})
export class AppAHrModule { }
