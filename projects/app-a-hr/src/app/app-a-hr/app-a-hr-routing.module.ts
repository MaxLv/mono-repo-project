import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppAHrContainerComponent } from '../components/app-a-hr/app-a-hr-container/app-a-hr-container.component';

const routes: Routes = [
  {
    // path: 'app-a-hr', component: AppAHrContainerComponent
    path: '', component: AppAHrContainerComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AppAHrRoutingModule {
  constructor() {
    console.log('App A HR AppAHrRoutingModule');

  }
}
