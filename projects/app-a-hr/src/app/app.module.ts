import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';

import { AppComponent } from './app.component';
import { AppAHrContainerComponent } from './components/app-a-hr/app-a-hr-container/app-a-hr-container.component';
import { AppAHrModule } from './app-a-hr/app-a-hr.module';

@NgModule({
  declarations: [
    AppComponent,
    AppAHrContainerComponent
  ],
  imports: [
    //!! must remove BrowserModule,
    AppAHrModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModuleAppAHr {
  constructor() {
    console.log('Projec A HR AppModuleAppAHr');

  }
}

@NgModule({})
export class AppAHrSharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AppModuleAppAHr,
      providers: []
    }
  }
}