import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-app-a-hr-container',
  templateUrl: './app-a-hr-container.component.html',
  styleUrls: ['./app-a-hr-container.component.css']
})
export class AppAHrContainerComponent implements OnInit {

  title: string = 'Project A Project-A-HR Module';

  constructor() { }

  ngOnInit() {
  }

}
