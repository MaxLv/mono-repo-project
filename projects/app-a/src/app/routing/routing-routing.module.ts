import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from '../app.component';
import { AppAHrSharedModule } from 'projects/app-a-hr/src/app/app.module';
// import { AppModuleAppAHr } from 'projects/app-a-hr/src/app/app.module';

const routes: Routes = [
  {
    path: '', component: AppComponent
  },
  {
    // path: 'hr', loadChildren: '../hr/hr.module'
    path: 'hr', loadChildren: '../hr/hr.module#HrModule'
    // path: 'hr', component:HrComponent
  },
  {
    path: 'reports', loadChildren: '../reports/reports.module#ReportsModule'
    // path: 'reports', loadChildren: '../reports/reports.module'
  },
  {
    path: 'app-a-hr', loadChildren: 'projects/app-a-hr/src/app/app.module#AppModuleAppAHr'
    // path: 'app-a-hr', loadChildren: 'projects/app-a-hr/src/app/app.module#AppAHrSharedModule'
  }
];

@NgModule({
  imports: [
    // RouterModule.forRoot(routes),
    RouterModule.forChild(routes),
    // AppAHrSharedModule.forRoot()
  ],
  exports: [
    RouterModule
  ]
})
export class RoutingRoutingModule {
  constructor() {
    console.log('Project A RoutingRoutingModule');

  }
}
