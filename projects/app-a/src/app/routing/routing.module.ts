import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoutingRoutingModule } from './routing-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RoutingRoutingModule
  ]
})
export class RoutingModuleProjectA {
  constructor() {
    console.log('Project A RoutingModule');

  }
}
