import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HrComponent } from './hr.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: HrComponent
  }
]

@NgModule({
  declarations: [
    HrComponent
  ],
  imports: [
    CommonModule,
    // ! NOT WORKING RouterModule.forRoot(routes)
    RouterModule.forChild(routes)
  ],
  exports:[
    RouterModule
  ]

})
export class HrModule {
  constructor() {
    console.log('Project-A HrModule');

  }
}
