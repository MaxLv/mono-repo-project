import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hr',
  templateUrl: './hr.component.html',
  styleUrls: ['./hr.component.css']
})
export class HrComponent implements OnInit {

  title: string = 'Project A HR Module';

  constructor() {
    console.log('HrComponent');

  }

  ngOnInit() {
  }

}
