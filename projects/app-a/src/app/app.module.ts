import { NgModule, ModuleWithProviders } from '@angular/core';
import { AppComponent } from './app.component';
import { RoutingModuleProjectA } from './routing/routing.module';
import { RouterModule } from '@angular/router';
import { NavComponent } from './nav/nav.component';
import { CommonModule } from '@angular/common';
import { HrModule } from './hr/hr.module';
import { ReportsModule } from './reports/reports.module';
@NgModule({
  declarations: [
    AppComponent,
    NavComponent
  ],
  imports: [
    CommonModule,
    RoutingModuleProjectA,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
// export class AppModule {
export class AppModuleProjecA {
  constructor() {
    console.log('Project A AppModule');
  }
}

@NgModule({})
export class ProjectASharedModule {
  constructor() {
    console.log('ProjectASharedModule');
  }
  static forRoot(): ModuleWithProviders {
    return {
      // ngModule: AppModule
      ngModule: AppModuleProjecA
    }
  }
}