import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeesComponent } from './components/employees/employees.component';
import { Routes, RouterModule } from '@angular/router';
import { CoreModule } from '../core/core.module';
import { EmployeeDetailsComponent } from '../employee-details/employee-details.component';

const routes: Routes = [
  {
    path: '', component: EmployeesComponent,
    children: [
      // {
      //   path: 'employees', component: EmployeesComponent
      // },
      {
        // path: 'details', loadChildren: '../employee-details/employee-details.module#EmployeeDetailsModule'
        path: ':name', loadChildren: '../employee-details/employee-details.module#EmployeeDetailsModule'
        // path: 'employees/:name', component: EmployeeDetailsComponent
        // path: ':name', component: EmployeeDetailsComponent
      }
    ]
  }
];

@NgModule({
  declarations: [
    EmployeesComponent,
    // EmployeeDetailsComponent
  ],
  imports: [
    CommonModule,
    CoreModule,
    RouterModule.forChild(routes)
  ]
})
export class EmployeesModule {
  constructor() {
    console.log('Main Project EmployeesModule');

  }
}
