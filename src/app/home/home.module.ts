import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { Routes, RouterModule } from '@angular/router';
import { CoreModule } from '../core/core.module';

const routes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'employees', loadChildren: '../employees/employees.module#EmployeesModule'
    // path:'employees',component:EmployeesComponent
  }
];

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    CoreModule,
    RouterModule.forChild(routes)
  ]
})
export class HomeModule {
  constructor() {
    console.log('Main Project HomeModule');

  }
}
