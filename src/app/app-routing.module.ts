import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { EmployeesComponent } from './employees/components/employees/employees.component';
import { ProjectASharedModule } from 'projects/app-a/src/app/app.module';

const routes: Routes = [
  {
    path: '', loadChildren: '../app/home/home.module#HomeModule'
  },
  {
    // !! Module Not Found !! path: 'employees', loadChildren: 'src/app/employees/employees.module#EmployeesModule'
    path: 'employees', loadChildren: 'src/app/employees/employees.module#EmployeesModule'
    // path: '', loadChildren: 'src/app/employees/employees.module#EmployeesModule'
  },
  {
    // !! Module Not Found !! path: 'app-a', loadChildren: '../../projects/app-a/src/app/app.module#ProjectASharedModule'
    // path: 'app-a', loadChildren: '../../projects/app-a/src/app/app.module#AppModule'
    path: 'app-a', loadChildren: '../../projects/app-a/src/app/app.module#AppModuleProjecA'
  },
  {
    path: '', redirectTo: '', pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    // ProjectASharedModule.forRoot()
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
